// INCLUDE FUNCTION
var rangeSlider = $(".slider-range"),
	range 		= $(".range"),
	tooltipster = $(".tooltip"),
	datepicker  = $(".date-picker"),
	styler 		= $(".styler"),
	popup 		= $("[data-popup]"),
	validate 	= $(".form");
	// console.log(datepicker);



var menu_selector = ".menu"; 

function onScroll(){
    var scroll_top = $(document).scrollTop();
    $(menu_selector + " a[href*='#']").each(function(){
        var hash = $(this).attr("href"),
        	wH = $(window).height();
        
        if($(hash).length){
	        
	        var target = $(hash);
	        if (target.position().top <= scroll_top+wH/2 && target.position().top + target.outerHeight() > scroll_top+wH/2) {
	            $(menu_selector + " a.active").removeClass("active");
	            $(this).addClass("active");
	        } else {
	            $(this).removeClass("active");
	        }

        }

    });
}

$(document).ready(function(){

	/* ------------------------------------------------
	VALIDATE START
	------------------------------------------------ */
		if(validate.length){
			$('#form--registration').validate({
				rules: {
					password: {
						required: true,
						minlength: 5
					},
					email: {
						required: true,
						email: true
					},
					recaptcha: {
						required: true,
						//recaptcha: true,
					}
				},

				messages:{

                    password:{
                        required: "Это поле обязательно для заполнения",
                        minlength: "Введите минимум 5-ть символов",
                    },

                    email:{
                        required: "Это поле обязательно для заполнения",
                        email: "Адрес e-mail указан некорректно",
                    },
                    recaptcha:{
                        required: "Это поле обязательно для заполнения",
                        //srecaptcha: "Введенные символы указаны неправильно"
                    },

                },

			});
		}
		if(validate.length){
			$('#form--authorization').validate({
				rules: {
					password: {
						required: true,
						minlength: 5
					},
					email: {
						required: true,
						email: true
					},
					recaptcha: {
						required: true,
						//recaptcha: true,
					},
					googleAuthenticator: {
						required: true,
						//recaptcha: true,
					}
				},

				messages:{

                    password:{
                        required: "Это поле обязательно для заполнения",
                        minlength: "Введите минимум 5-ть символов",
                    },

                    email:{
                        required: "Это поле обязательно для заполнения",
                        email: "Адрес e-mail указан некорректно",
                    },
                    recaptcha:{
                        required: "Это поле обязательно для заполнения",
                        //srecaptcha: "Введенные символы указаны неправильно"
                    },
                    googleAuthenticator:{
                        required: "Это поле обязательно для заполнения",
                        //googleAuthenticator: "Google Authenticator указан некорректно"
                    },

                },

			});
		}
		if(validate.length){
			$('#form--recovery').validate({
				rules: {
					password: {
						required: true,
						minlength: 5
					},
					email: {
						required: true,
						email: true
					},
					recaptcha: {
						required: true,
						//recaptcha: true,
					},
				},
				messages:{

                    password:{
                        required: "Это поле обязательно для заполнения",
                        minlength: "Введите минимум 5-ть символов",
                    },

                    email:{
                        required: "Это поле обязательно для заполнения",
                        email: "Адрес e-mail указан некорректно",
                    },
                    recaptcha:{
                        required: "Это поле обязательно для заполнения",
                        //srecaptcha: "Введенные символы указаны неправильно"
                    },

                },
			});
		}
	/* ------------------------------------------------
	VALIDATE END
	------------------------------------------------ */



	/* ------------------------------------------------
	RANGE-SLIDER START
	------------------------------------------------ */

		if(range.length){

			$(".range").each(function(index, el) {
				var $this = $(el),
					input = $this.closest('.range__wrap').find('.amount'),
					config = $this.data("config");

				$this.ionRangeSlider({
		            type: "single",
		            hide_min_max: true,
		            keyboard: true,
		            grid: false,
		            prettify_enabled: true,
		            min: config.min,
		            max: config.max,
		            from: config.from,
		            step: config.step,
		            onStart: function(data) {
		            	var qt = $this.data("from") +'';
		            	input.val(qt.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));
		            },
		            onChange: function( event ) {
		            	var qt = $this.data("from") +'';
		                input.val(qt.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));
		            }
		        });

				var slider = $this.data("ionRangeSlider");

				input.bind("change keyup input click", function() {
				    if (this.value.match(/[^0-9 .]/g)) {
				        this.value = this.value.replace(/[^0-9 .]/g, '').replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
				    }
				    else{
				    }
				});

				input.on('blur', function() {

					var inputVal = $(this).val()+'',
						from = $(this).val().replace(/\s+/g, '');

					slider.update({
			    		from:from
			    	});

					$(this).val(inputVal.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));
				
				})
		     //    input.on( "change", function() {
			    // 	
			    // });

			})			

		};
	/* ------------------------------------------------
	RANGE-SLIDER END
	------------------------------------------------ */



	/* ------------------------------------------------
	TOOLTIP START
	------------------------------------------------ */
		if(tooltipster.length){
			tooltipster.tooltipster();
		}
	/* ------------------------------------------------
	TOOLTIP END
	------------------------------------------------ */



	/* ------------------------------------------------
	DATEPICKER START
	------------------------------------------------ */

		if(datepicker.length){
			datepicker.datepicker({
				firstDay: 1,
				showOtherMonths: true,
				onClose: function(){
					$('body').removeClass('date_active');
				}
			});
		}

	/* ------------------------------------------------
	DATEPICKER END
	------------------------------------------------ */



	/* ------------------------------------------------
	STYLER START
	------------------------------------------------ */

		if (styler.length){
	 		styler.styler();
	 	}

	/* ------------------------------------------------
	STYLER END
	------------------------------------------------ */



	/* ------------------------------------------------
	POPUP START
	------------------------------------------------ */

		if(popup.length){
			popup.on('click',function(){
			    var modal = $(this).data("popup");

			    $(modal).arcticmodal({
			    	beforeOpen: function(){
			    		
			    	}
			    });
			});
		};

	/* ------------------------------------------------
	POPUP END
	------------------------------------------------ */


	/* ------------------------------------------------
	PARTICLES START
	------------------------------------------------ */

		if($('.particles').length){
			particlesJS("particles-js", {"particles":{"number":{"value":80,"density":{"enable":true,"value_area":800}},"color":{"value":"#ffffff"},"shape":{"type":"circle","stroke":{"width":0,"color":"#000000"},"polygon":{"nb_sides":5},"image":{"src":"img/github.svg","width":100,"height":100}},"opacity":{"value":0.5,"random":false,"anim":{"enable":false,"speed":1,"opacity_min":0.1,"sync":false}},"size":{"value":3,"random":true,"anim":{"enable":false,"speed":40,"size_min":0.1,"sync":false}},"line_linked":{"enable":true,"distance":150,"color":"#ffffff","opacity":0.4,"width":1},"move":{"enable":true,"speed":2,"direction":"none","random":false,"straight":false,"out_mode":"out","bounce":false,"attract":{"enable":false,"rotateX":600,"rotateY":1200}}},"interactivity":{"detect_on":"canvas","events":{"onhover":{"enable":false,"mode":"repulse"},"onclick":{"enable":false,"mode":"push"},"resize":true},"modes":{"grab":{"distance":400,"line_linked":{"opacity":1}},"bubble":{"distance":400,"size":40,"duration":2,"opacity":8,"speed":3},"repulse":{"distance":200,"duration":0.4},"push":{"particles_nb":4},"remove":{"particles_nb":2}}},"retina_detect":true});var count_particles, stats, update; stats = new Stats; stats.setMode(0); stats.domElement.style.position = 'absolute'; stats.domElement.style.left = '0px'; stats.domElement.style.top = '0px'; document.body.appendChild(stats.domElement); count_particles = document.querySelector('.js-count-particles'); update = function() { stats.begin(); stats.end(); requestAnimationFrame(update); }; requestAnimationFrame(update);;
		}

	/* ------------------------------------------------
	PARTICLES END
	------------------------------------------------ */


	/* ------------------------------------------------
	MENU ANCHOR
	------------------------------------------------ */

		

		$(document).on("scroll", onScroll);
 
	    $("a[href^=#anchor--]").click(function(e){
	        e.preventDefault();
	 
	        $(document).off("scroll");
	        $(menu_selector + " a.active").removeClass("active");
	        $(this).addClass("active");
	        var hash = $(this).attr("href");
	        var target = $(hash);
	 
	        $("html, body").animate({
	            scrollTop: target.offset().top
	        }, 500, function(){
	            window.location.hash = hash;
	            $(document).on("scroll", onScroll);
	        });
	 
	    });

	/* ------------------------------------------------
	MENU ANCHOR END
	------------------------------------------------ */



	$('.date-picker').on('click', function(){
		var body = $('body');

		body.addClass('date_active');
	});


	$(document).mouseup(function (e){ 
		var div = $('.date-picker'),
			dp  = $('.ui-datepicker'); 

		if (!div.is(e.target) 
		    && div.has(e.target).length === 0  && !dp.is(e.target) && dp.has(e.target).length === 0 ) { 
			var body = $('body');
			body.removeClass('date_active');
		}

		var parent = $('.lang');
		if (!parent.is(e.target) 
		    && parent.has(e.target).length === 0 ) { 
			parent.removeClass('lang_opened');
		}
	});


	$('.lang, .lang__link').on('click', function(e){
		var $this = $(this);
		$this.toggleClass('lang_opened');

		e.preventDefault();
	});

	$('.ui-datepicker-calendar td a').on('click', function(){
		var body = $('body');

		body.removeClass('date_active');
	});

	$(".menu_btn, .menu_close").on('click ontouchstart', function(e){
  		var body = $('body');
  		
  		body.toggleClass('nav_overlay');
    });

	$(".authorized--submenu_link, .authorized--submenu--close").on('click ontouchstart', function(e){
  		var body = $('body');
  		
  		body.toggleClass('submenu_overlay');
    })




	
})